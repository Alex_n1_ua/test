app.directive('listForm', function(ListService, $route) {
	return {
		restrict: 'E',
		scope:{
			items	: '='
			},
		templateUrl: 'listForm.html',
		link: function(scope) {	

			if($route.current.params.id != 'null'){
				index = $route.current.params.id;
        		scope.newItem = scope.items[index];
			}

			scope.addItem = function(){
				if(scope.newItem.name != '' && scope.newItem.name != null){
					if(scope.newItem.id == null){
						scope.newItem.id = 1 + scope.items[scope.items.length - 1].id ;
						scope.items.push(scope.newItem);
					} 

					scope.newItem = {};
					ListService.set(scope.items);
				}
			}
		}	
	}
});