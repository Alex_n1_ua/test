app.factory('ListService', function() {

	get = function() {
			return JSON.parse(localStorage.getItem(LIST_KEY)) || [];
		},

	set = function(item) {
			localStorage.setItem(LIST_KEY, JSON.stringify(item));
		};

	return {
			set : set,
			get	: get
	}
});