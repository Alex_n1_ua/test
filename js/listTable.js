app.directive('listTable',  function(ListService) {
	return {
		restrict: 'E',
		scope:{
			items: '='
			},
		templateUrl: 'listTable.html',
		link: function(scope) {

			scope.removeItem = function (index){
		        scope.items.splice(index, 1);
		        ListService.set(scope.items);
		    };

		}
	}
});