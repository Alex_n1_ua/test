var LIST_KEY = '12345';

var app = angular.module("listApp", ["ngRoute"]);

app.config(function($routeProvider) {
    $routeProvider
    .when("/", {
        templateUrl : "list.html"
    })
    .when("/list", {
        templateUrl : "list.html"       
    })
    .when("/list/add", {
        templateUrl : "listAdd.html"
    }).
    when('/list/edit/:id', {
        templateUrl : "listAdd.html"  
    });
});

app.controller('listCtrl', ['$scope', 'ListService',
    function($scope, ListService) {

        $scope.newItem = {};
        $scope._list = ListService.get();

        if( !$scope._list.length ){
            ListService.set([{name :'TEST ITEM 1', id : 1},{name :'TEST ITEM 2',id : 2}]);
            $scope._list = ListService.get();
        }
}]);

