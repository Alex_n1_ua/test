var app = angular.module('grafApp', ['chart.js']);
app.controller('GrafCtrl', function($scope) {
 
    $scope.typeChart    = 'table1';
    $scope.table1       = [];
    $scope.table2       = [];
    $scope.labels1      = [0,1,2,3,4,5,6,7,8,9,10];
    $scope.data1        = [];
    $scope.labels2      = [];
    $scope.data2        = [];

});

app.directive('table',  function() {
    return {
    restrict: 'A',
    templateUrl: 'tableData.html',
    link: function(scope) {

            for (var i = 1; i <= 5; i++) {
                x = Math.floor((Math.random() * 10) + 1);
                if(scope.data1[x] != null) {
                    --i;
                    continue;
                }
                y = Math.floor((Math.random() * 10) + 1);
                scope.table1.push({'x': x, 'y': y});
                scope.data1[x] = y;
            }

            for (var i = 1; i <= 5; i++) {
                x = i*2;
                y = Math.floor((Math.random() * 10) + 1);
                scope.table2.push({'x': x, 'y': y});
                scope.data2.push(y);
                scope.labels2.push(x);
            }
        }
    }
});

app.directive('graf',  function() {
    return {
    restrict: 'A',
    templateUrl: 'graf.html',
    link: function(scope) {

            scope.options = {
                showLines : false,
                scales: {
                    yAxes: [{
                        ticks: {
                            max: 10,
                            min: 0,
                            stepSize: 1
                        }
                    }]
                }
            };

            scope.togleGraf = function(){
                if(scope.typeChart == 'table1'){
                    scope.typeChart = 'table2'
                } else {
                    scope.typeChart = 'table1'
                }
            }
        }
    }
});

